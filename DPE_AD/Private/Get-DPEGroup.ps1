function Get-DPEGroup {
    param (

    )
    $groupName = Read-Host "Enter the group name or wildcard search string"

    $group = (Get-ADGroup -Filter "Name -like '*$groupName*'").Name | Out-GridView -Title 'Select one or more AD groups' -PassThru

    $group
}