function Get-UserID {

    [CmdletBinding()]
    param (
        [string] $Username
    )
    
    BEGIN {

        $domain = (((Get-ADDomain).name).toUpper())

    }

    PROCESS {
        
        # Check all user accounts that match the name provided return only those that are in a state of enabled
        Write-Verbose "Checking for user accounts that match $username"
        $identity = Get-ADUser -Filter "(DisplayName -like '$username') -and (Enabled -eq 'True')"

        # Check to see if user object found that matches search criteria
        if ($null -eq $identity) {
            Write-Warning "No user found in $domain AD with that name, checking for user accounts that match the surname"

            # Search for accounts using the surname of the user provided
            Write-Verbose "Checking for accounts matching the provided users surname"
            $surname = ($username.Split('')[1])
            $identity = Get-ADUser -Filter { Surname -like $surname } -Properties DistinguishedName, GivenName, Surname, DisplayName, Name | Select-Object Name, GivenName, SamAccountName, Surname, DisplayName, UserPrincipalName, DistinguishedName

            # Check to see if user object found that matches the surname
            if ($null -eq $identity) {
                Write-Warning "No user account for $Username found in $domain AD, check the username spelling"
                break
            }
        
            # If multiple matches on the surname found output to window to select user account
            elseif ($identity.count -gt 1) {
                Write-Verbose "Select the user name from the output window to continue"
                $identity = (($identity | Out-GridView -PassThru).SamAccountName)
                $identity
            } 
            else {
                $name = $identity.DisplayName
                Write-Information "One user account matching the surname found: $name" -InformationAction Continue
                $identity = $identity.SamAccountName
                $identity
            }
        }
        
        # If multiple matches on the users name then output to window to select user account
        elseif ($identity.count -gt 1) {
            Write-Verbose "Select the user name from the output window to continue"
            $identity = (($identity | Out-GridView -PassThru).SamAccountName)
            $identity
        }
        
        else {
            $identity = $identity.SamAccountName
            $identity
        }

    }

    END {

    }

}
