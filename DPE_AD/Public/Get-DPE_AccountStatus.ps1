function Get-DPE_AccountStatus {
    
    <#
    .SYNOPSIS
    Get the users the Active Directory user account status and writes this 
    information out to the console.
    .DESCRIPTION
    Get-DPE_AccountStatus, uses the PowerShell Active Directory module 
    to retrieve the current account status of the user or users provided.
    The function returns key account information including if the AD account 
    is Enabled, LockedOut, Expired, and if their password has expired, and 
    the date it was last set. 
    If the account is disabled, password expired, or locked out a warning is 
    written to the console.
    
    This function is primarily useful when you want to quickly confirm the 
    status of a users AD account.
    
    Note: If the users name is spelt incorrectly or cannot be resolved a warning
    message will be output to the console.
    
    .PARAMETER Username
    The Username parameter specifies the first and last name of the Active 
    Directory user to query the name should be inside apostrophes.
    .EXAMPLE
    Get-DPE_AccountStatus -Usersname 'John Smith'
    
    This command gets the Active Directory account status of the user you specify
    .EXAMPLE
    Get-DPE_AccountStatus -Usersname 'John Smith', 'David Arthurson'
    
    This command gets the Active Directory account status of the users you specify
    .EXAMPLE
    Get-DPE_AccountStatus -Usersname (Get-Content c:\TMP\DPE_ADUsers.txt) -Verbose
    
    This command gets the Active Directory user account status of the users contained in the 
    DPE_ADUsers.txt file, as verbose is specified additional information is written to the console
    #>
    
        [CmdletBinding()]
        param(
            [Parameter(Mandatory = $True, 
                ValueFromPipeline = $True,
                HelpMessage = "Users first and last name in quotes")]
            [Alias('name')]
            [string[]]$Usersname
        )
        BEGIN {
            if (!(Get-Module ActiveDirectory)) {
                Import-Module ActiveDirectory
            }
    
            $domain = (((Get-ADDomain).name).toUpper())
            
            $date = Get-Date
        }
    
        PROCESS {
            foreach ($user in $Usersname) {
                Write-Verbose "Querying $user's AD account status"
                $SAM = Get-ADUser -Filter { Displayname -like $user } -Properties "DisplayName", "SamAccountName" , "Department", `
                    "Enabled", "LockedOut", "PasswordExpired", "PasswordLastSet", "msDS-UserPasswordExpiryTimeComputed", "BadPasswordTime", "BadPwdCount", "LastLogon"#| Where-Object Enabled -EQ $True
                
                if ($null -eq $SAM) {
                    Write-Warning "No user account for $user found in $domain AD."
                }
                else {
    
                    foreach ($status in $SAM) {
    
                        try {
                            $PasswordExpiry = [datetime]::FromFileTime($status.'msDS-UserPasswordExpiryTimeComputed')
                            Write-Debug -Message "Checking value of PasswordExpiry variable"
                        }
                        catch {
                            if (!$PasswordExpiry) {
                                $Expiry = "Password Expiry Date cannot be validated"
                            }
                        }
                        
                        $props = [Ordered]@{'DisplayName' = $status.DisplayName;
                            'Username'                    = $status.SamAccountName;                
                            'Department'                  = $status.Department;
                            'Enabled'                     = $status.Enabled;
                            'Locked Out'                  = $status.LockedOut;
                            'Password Expired:'           = $status.PasswordExpired;
                            'Password Last Set:'          = $status.PasswordLastSet;
                            'Password Expiry Date:'       = $PasswordExpiry;
                            'Bad Password Time:'          = [DateTime]::FromFileTime($_.BadPasswordTime);
                            'Bad Password Count:'         = $status.BadPwdCount;
                            'Last Logon:'                 = [DateTime]::FromFileTime($_.LastLogon)
                        }

                        Write-Output "AD Query executed at $date`n"
        
                        $obj = New-Object -TypeName PSObject -Property $props
                        Write-Output $obj
                        if ($status.Enabled -eq $false) {
                            Write-Warning -Message "$user's AD account is Disabled"
                        }
                        if ($status.PasswordExpired -eq $true) {
                            Write-Warning -Message "$user's password has Expired"
                        }
                        if ($status.LockedOut -eq $true) {
                            Write-Warning -Message "$user's account is currently LockedOut"
                        }
                        if ($Expiry) {
                            Write-Warning $Expiry
                        }
                        Write-Output "`n"

                    }
    
                }
    
            }

            Set-Clipboard $status.SamAccountName
            
            Write-Verbose "AD queries now complete"
        }
    
        END { }
    }
    