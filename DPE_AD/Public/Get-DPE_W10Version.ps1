function Get-DPE_W10Version {

    <#
    .SYNOPSIS
    Gets the chosen devices Windows 10 version and writes this information
    out to the console.
    .DESCRIPTION
    Get-DPE_W10Version, uses the PowerShell Active Directory module to
    retrieve the Windows 10 version number from the computer object in
    Active Directory.
    The function supports querying one or more devices. The device names
    need to be seperated by commas. The option to include the device name
    in the output written to the console is supported via the 
    includeComputerName parameter.
    By default the function writes the Windows 10 version name for the
    device you queried to the console.
    
    This function is primarily useful when you want to quickly confirm the
    version of Windows 10 a device is currently using.
        
    .PARAMETER computerName
    The computerName parameter specifies the name of the Active Directory
    computer object to query.
    .PARAMETER includeComputerName
    The includeComputerName parameter includes the name of the device in 
    the information that is written to the console. This is helpful when
    querying multiple devices.
    .EXAMPLE
    Get-DPE_W10Version -computerName PC0V3MBM
    
    This command gets the Active Directory Windows 10 version number of the
    computer object you specify.
    .EXAMPLE
    Get-DPE_W10Version -computerName PC0V3MBM, PC0V3MZC -includeComputerName
    
    This command gets the Active Directory Windows 10 version number of the
    computer object you specify and includes the device name in the informatio
    that is written to the console.
    #>

    [CmdletBinding()]
    param (
        [Parameter (Mandatory=$true,
                    ValueFromPipeline=$true)]
        [string[]] $computerName,

        [switch] $includeComputerName
    )
    
    begin {
        if (!(Get-Module ActiveDirectory)) {
            Import-Module ActiveDirectory
        }
    }
    
    process {
        Write-Verbose "Beginning Process Block"
        foreach ($computer in $computername){
            Write-Verbose "Querying $computer"
            try {
                $everything_ok = $true
                $OS = (Get-ADComputer -Identity $computer -Properties OperatingSystemVersion -ErrorAction Stop).OperatingSystemVersion
            }
            catch {
                $everything_ok=$false
                Write-Warning "$computer not found in AD check the device name was entered correctly"
            }

            if ($everything_ok) {
                $version = Switch ($OS) {
                    '10.0 (14393)' {"Windows 10 1607"}
                    '10.0 (16299)' {"Windows 10 1709"}
                    '10.0 (17134)' {"Windows 10 1803"}
                    '10.0 (17763)' {"Windows 10 1809"}
                    '10.0 (18362)' {"Windows 10 1903"}
                    '10.0 (18363)' {"Windows 10 1909"}
                    '10.0 (19041)' {"Windows 10 2004"}
                    '10.0 (19042)' {"Windows 10 20H2"}
                    '10.0 (19044)' {"Windows 10 21H2"}
                    '10.0 (19045)' {"Windows 10 22H2"}
                    Default {"Unknown version of Windows 10"}
                }

                if ($includeComputerName) {
                    "$computer" + ": $version"
                }
                else {
                    $version
                }
            }
        }
    }
    
    end {
        
    }
}