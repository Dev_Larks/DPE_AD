function Get-DPE_VPNConfigurationState {

    <#
    .SYNOPSIS
    Checks the nominated users and device 'DPE - Always on VPN' configuration state in
    Active Directoryand writes this information out to the console.
    .DESCRIPTION
    Get-DPE_VPNConfigurationState, uses the PowerShell Active Directory module 
    to retrieve the current 'DPE - Always on VPN' user and device configuation state.
    The function outputs the result of the queries in coloured text to the 
    console, green text indicates the Active Directory group is present, Orange
    text indicates that the group is missing. 
    To remediate missing groups the function then prompts you to provide 
    administrator credentials and adds the missing group to the user/device 
    account. 
    This functionality can be disabled by using the -noRemediate parameter which
    just outputs the user/device configuration state to the console.
    
    This function is primarily useful when you want to quickly confirm the Active
    Directory configuration state of a user and device.
    
    Note: If the users name is spelt incorrectly or cannot be resolved a warning
    message will be output in the console.
    
    .PARAMETER userName
    The Username parameter specifies the first and last name of the Active 
    Directory user to query the name should be inside apostrophes.

    .PARAMETER computerName
    The computerName parameter specifies the Active Directory computer serial
    number to query.

    .PARAMETER noRemediate
    The noRemediate parameter specifies that the state of the user/device should
    be queried only with no remediation steps being completed.

    .EXAMPLE
    Get-DPE_VPNConfigurationState -userName 'Craig Larkin' -computerName PC0V3MBM -noRemediate -Verbose

    VERBOSE: Querying Craig Larkin's VPN settings
    VERBOSE: Checking for user accounts that match Craig Larkin
    VERBOSE: Querying AD for Craig Larkin's 'DPE - Always On VPN' configuration state

    G-SE-CORP-INTUNE-WIN10-Enrol-Users AD group missing
    G-SE-CORP-VPN-Users AD group present

    VERBOSE: Querying AD for PC0V3MBM's 'DPE - Always On VPN' configuration state
    G-SE-CORP-INTUNE-AOVPN-DEVICE-TUNNEL AD group present
    G-SE-CORP-INTUNE-AOVPN-USER-TUNNEL-v2.3 AD group present
    G-SE-CORP-INTUNE-COMANAGED-DEVICES AD group present

    VERBOSE: Querying AD for PC0V3MBM's OU location
    PC0V3MBM is in the correct OU: 'Hybrid-Joined' for 'DPE - Always On VPN'

    This command gets the 'DPE - Always On VPN' Active Directory configuration state of the
    user and device you specify. As the no remediate parameter is specified missing
    groups are written to the console only.
    .EXAMPLE
    Get-DPE_VPNConfigurationState -userName 'Craig Larkin' -computerName PC0V3MBM -Verbose
    
    VERBOSE: Querying Craig Larkin's VPN settings
    VERBOSE: Checking for user accounts that match Craig Larkin
    VERBOSE: Querying AD for Craig Larkin's 'DPE - Always On VPN' configuration state

    G-SE-CORP-INTUNE-WIN10-Enrol-Users AD group present
    G-SE-CORP-User-Cert AD group present
    VERBOSE: G-SE-CORP-User-Cert-NDES AD group missing adding group
    cmdlet Get-Credential at command pipeline position 1
    Supply values for the following parameters:
    User: admin-larkinc
    Password for user admin-larkinc: **********
    G-SE-CORP-VPN-Users AD group present

    VERBOSE: Querying AD for PC0V3MBM's 'DPE - Always On VPN' configuration state
    G-SE-CORP-INTUNE-AOVPN-DEVICE-TUNNEL AD group present
    G-SE-CORP-INTUNE-AOVPN-USER-TUNNEL-v2.3 AD group present
    G-SE-CORP-INTUNE-COMANAGED-DEVICES AD group present

    VERBOSE: Querying AD for PC0V3MBM's OU location
    PC0V3MBM is in the correct OU: 'Hybrid-Joined' for 'DPE - Always On VPN' 

    This command gets the Active Directory account status of the user and device you specified. As
    the no remediate parameter is not used if AD groups are found to be not present you are prompted
    for admin credentials for each missing group to remediate the Active Directory AOVPN 
    configuration into the correct state.    
    #>

    [CmdletBinding()]
    param (

        [Parameter (Mandatory = $True)]
        [string] $userName,

        [Parameter(Mandatory = $True)]
        [string] $computerName,

        [switch] $noRemediate
    )
    
    begin {
        if (!(Get-Module ActiveDirectory)) {
            Import-Module ActiveDirectory
        }
    }
    
    process {
        Write-Verbose "Querying $userName's VPN settings"

        $vpnUserGroups = 'G-SE-CORP-INTUNE-WIN10-Enrol-Users', 'G-SE-CORP-VPN-Users'

        # Call private helper function to get UserID
        $userId = Get-UserID -userName $userName

        try {
            $everything_ok = $True
            "`r"
            Write-Verbose "Querying AD for $userName's 'DPE - Always On VPN' configuration state`n"
            $userVpn = Get-ADPrincipalGroupMembership $userId | Select-Object -ExpandProperty name -ErrorAction Stop
        }
        catch {
            if ($everything_ok = $False) { break }
            Write-Warning "No user account found in DPE AD for $userName`n"
        }
        foreach ($userGroup in $vpnUserGroups) {
            
            if ($everything_ok) {
                if ($userVpn -Contains $userGroup) {
                    Write-Host "$userGroup AD group present" -ForegroundColor Green
                }
                else {
                    if ($noRemediate) {
                        Write-Warning "$userGroup AD group missing"
                    }
                    else {
                        if (!$AdminCredDEC) {
                            $AdminCredDEC = Get-Credential
                        }
                        Write-Host "$userGroup AD group missing - " -ForegroundColor DarkYellow -NoNewline; Write-Host " Adding group" -ForegroundColor Green
                        Add-ADGroupMember -Identity $userGroup -Members $userID -Credential $AdminCredDEC
                        Write-Verbose "$userGroup successfully added to user AD account"
                    }
                }
            }
        }
        
        $vpnDeviceTunnelGroups = 'G-SE-CORP-INTUNE-AOVPN-DEVICE-TUNNEL-Batch1', 'G-SE-CORP-INTUNE-AOVPN-DEVICE-TUNNEL-Batch2', 'G-SE-CORP-INTUNE-AOVPN-DEVICE-TUNNEL-Batch3', 'G-SE-CORP-INTUNE-AOVPN-DEVICE-TUNNEL-IT', 'G-SE-CORP-INTUNE-AOVPN-DEVICE-TUNNEL-Pilot'
        "`r"
        try {
            $everything_ok = $True
            Write-Verbose "Querying AD for $computerName's 'DPE - Always On VPN' configuration state`n"
            $deviceVpn = Get-ADPrincipalGroupMembership $computerName$ | Select-Object -ExpandProperty name -ErrorAction Stop
        }
        catch {
            if ($everything_ok = $false) { break }
            Write-Warning "No computer object found in DPE AD for $computerName check the spelling and try again`n" 
        }

        $deviceVPNGroups = $vpnDeviceTunnelGroups | where { $deviceVpn -contains $_ }

        #foreach ($deviceGroup in $deviceVPNGroups) {
            
        if ($deviceVPNGroups) {
            #$deviceGroup = $vpnDeviceTunnelGroups | where { $deviceVPNGroups -contains $_ }
            foreach ($group in $deviceVPNGroups) {
                Write-Host "$group AD group present" -ForegroundColor Green
            }
        }
        else {
            $deviceGroup = 'G-SE-CORP-INTUNE-AOVPN-DEVICE-TUNNEL-Batch1'
            $dtCleanup = 'G-SE-CORP-INTUNE-AOVPN-DEVICE-TUNNEL-Batch1-Cleanup'
            if ($noRemediate) {
                Write-Warning "$deviceGroup AD group missing"
            }
            else {
                if (!$AdminCredDEC) {
                    $AdminCredDEC = Get-Credential
                }
                Write-Host "$deviceGroup AD group missing" -ForegroundColor DarkYellow -NoNewline; Write-Host " Adding group" -ForegroundColor Green
                Add-ADGroupMember -Identity $deviceGroup -Members $computerName$ -Credential $AdminCredDEC
                Add-ADGroupMember -Identity $dtCleanup -Members $computerName$ -Credential $AdminCredDEC
                Write-Verbose "$deviceGroup successfully added to $computerName AD account"
                Write-Verbose "$dtCleanup successfully added to $computerName AD account"
            }
        }    


        $vpnUserTunnelGroups = 'G-SE-CORP-INTUNE-AOVPN-USER-TUNNEL-Prod', 'G-SE-CORP-INTUNE-AOVPN-USER-TUNNEL-v2.6'
        $deviceVPNGroups = $vpnUserTunnelGroups | Where-Object { $deviceVpn -contains $_ }
        if ($deviceVPNGroups) {
            $deviceGroup = $vpnUserTunnelGroups | Where-Object { $deviceVPNGroups -contains $_ }
            foreach ($group in $deviceGroup) {
                Write-Host "$group AD group present" -ForegroundColor Green
            }
        }
        else {
            $deviceGroup = 'G-SE-CORP-INTUNE-AOVPN-USER-TUNNEL-Prod'
            if ($noRemediate) {
                Write-Warning "$deviceGroup AD group missing"
            }
            else {
                if (!$AdminCredDEC) {
                    $AdminCredDEC = Get-Credential
                }
                Write-Host "$deviceGroup AD group missing" -ForegroundColor DarkYellow -NoNewline; Write-Host " Adding group" -ForegroundColor Green
                Add-ADGroupMember -Identity $deviceGroup -Members $computerName$ -Credential $AdminCredDEC
                Write-Verbose "$deviceGroup successfully added to $computerName AD account"
            }
        } 



        if ($deviceVPN -contains 'G-SE-CORP-INTUNE-COMANAGED-DEVICES') {
            Write-Host "G-SE-CORP-INTUNE-COMANAGED-DEVICES AD group present" -ForegroundColor Green
        }
        else {
            if ($noRemediate) {
                Write-Warning "G-SE-CORP-INTUNE-COMANAGED-DEVICES AD group missing"
            }
            else {
                if (!$AdminCredDEC) {
                    $AdminCredDEC = Get-Credential
                }
                Write-Host "G-SE-CORP-INTUNE-COMANAGED-DEVICES AD group missing - " -ForegroundColor DarkYellow -NoNewline; Write-Host " Adding group" -ForegroundColor Green
                Add-ADGroupMember -Identity $deviceGroup -Members $computerName$ -Credential $AdminCredDEC
                Write-Verbose "G-SE-CORP-INTUNE-COMANAGED-DEVICES successfully added to $computerName AD account"
            }
        }
        #}
        #}

        $cleanupOldAOVPNConnections = 'G-SE-CORP-INTUNE-AOVPN-USER-TUNNEL-v2.3', 'G-SE-CORP-INTUNE-AOVPN-DEVICE-TUNNEL', 'G-SE-CORP-INTUNE-AOVPN-DEVICE-TUNNEL-Prod', 'G-SE-CORP-INTUNE-AOVPN-DEVICE-TUNNEL-CleanUp'
        foreach ($group in $cleanupOldAOVPNConnections) {
            if ($deviceVpn -contains $group) {
                if ($noRemediate) {
                    Write-Host "$group AD group present" -ForegroundColor Red
                }
                else {
                        Write-Warning "$group present removing AD group"
                        if (!$AdminCredDEC) {
                            $AdminCredDEC = Get-Credential
                        }
                        Remove-ADGroupMember -Identity $group -Members $computerName$ -Credential $AdminCredDEC -Confirm:$false
                        Write-Host "$group now removed" -ForegroundColor Green
                }
            }
        }

        # Check device OU
        "`r"
        Write-Verbose "Querying AD for $computerName's OU location"
        $vpnOU = (Get-ADComputer $computerName -Properties CanonicalName).CanonicalName

        if ($vpnOU.Contains('Hybrid-Joined')) {
            Write-Host "$computerName is in the correct OU: 'Hybrid-Joined' for 'DPE - Always On VPN'" -ForegroundColor Green    
        }
        else {
            Write-Warning "$computerName is in the wrong OU for 'DPE - Always On VPN'"
        }
        "`r"
    }

    end {
        
    }
}