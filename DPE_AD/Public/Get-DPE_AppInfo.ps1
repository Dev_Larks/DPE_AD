function Get-DPE_AppInfo {

<#
    .SYNOPSIS
    Gets the chosen Active Directory groups 'Info' and 'Description' field
    text and writes this information out to the console.
    .DESCRIPTION
    Get-DPE_AppInfo, uses the PowerShell Active Directory module to retrieve
    the selected AD group or groups description and information text. The 
    function supports wildcard searches and provides a window to select the
    group/s you want to query if multiple values are returned.
    The function returns the values of these two fields to the console.
    
    This function is primarily useful when you want to quickly confirm what an
    AD groups purpose is.
    
    Note: If the 'info' or 'description' field output is truncated pipe the
    functions output to Format-List.
    
    .PARAMETER groupName
    The groupName parameter specifies either the name of the Active Directory
    group to query the name should be inside apostrophes. Note that this 
    parameter also supports wildcard searches for the string you nominate.
    .EXAMPLE
    Get-DPE_AppInfo -groupName 'John Smith'
    
    This command gets the Active Directory account status of the user you specify
    .EXAMPLE
    Get-DPE_AccountStatus -Usersname 'John Smith', 'David Arthurson'
    
    This command gets the Active Directory account status of the users you specify
    .EXAMPLE
    Get-DPE_AccountStatus -Usersname (Get-Content c:\TMP\DPE_ADUsers.txt) -Verbose
    
    This command gets the Active Directory user account status of the users contained in the 
    DPE_ADUsers.txt file, as verbose is specified additional information is written to the console
    #>

    [CmdletBinding()]
    param (
        
    )
    
    BEGIN {
        if (!(Get-Module ActiveDirectory)) {
            Import-Module ActiveDirectory            
        }
    }

    PROCESS {
        Write-Verbose "Beginning Process Block"

        # Call private helper function
        $groupName = Get-DPEGroup

        foreach ($group in $groupName){
            Write-Verbose "Querying $group"
            $info = Get-ADGroup -Identity $group -Properties description, info | Select-Object name, description, info

            $props = @{'GroupName'=$info.name;
                       'Description'=$info.description;
                       'Info'=$info.info}
            Write-Verbose "AD queries now complete"
            $obj = New-Object -TypeName psobject -Property $props

            Write-Output $obj
        }
    }
    
    END {

    }
}