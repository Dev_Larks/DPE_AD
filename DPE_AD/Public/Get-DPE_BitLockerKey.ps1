function Get-DPE_BitLockerKey {

    <#
    .SYNOPSIS
    Get the devices BitLocker unlock key from Active Directory and writes this information out to the console.
    .DESCRIPTION
    Get-DPE_BitLockerKey, uses the PowerShell Active Directory module to retrieve the last BitLocker key that was written to Active Directory.
    The function returns the computer you are queryings name, date the drive was encrypted and the BitLocker unlock key.

    As this information the function is retrieving is sensitive the function prompts you for your administrator credentials.
    
    This function is primarily useful when you want to quickly obtain the BitLocker unlock key for a device hard disk.
    
    Note: If the computer serial you specify cannot be found in AD a warning is written to the console.
    
    .PARAMETER computerName
    The computerName parameter specifies the device asset name that you want to query.

    .EXAMPLE
    Get-DPE_BitLockerKey -computerName PC0V3MBM

    cmdlet Get-Credential at command pipeline position 1
    Supply values for the following parameters:
    User: admin-larkinc
    Password for user admin-larkinc: **********

    Name                           Value
    ----                           -----
    ComputerName                   PC0V3MBM
    EncryptedDate                  22/01/2021 10:52:06 AM
    BitLockerKey                   719774-######-######-######-######-######-######-######
    
    This command gets the BitLocker key for the device that you specify from Active Directory

    .EXAMPLE
    Get-DPE_BitLockerKey -computerName PC0V3ZS4 -Verbose

    VERBOSE: Checking AD for PC0V3ZS4's Distinguished Name
    VERBOSE: Retrieving BitLocker key from Active Directory
    cmdlet Get-Credential at command pipeline position 1
    Supply values for the following parameters:
    User: admin-larkinc
    Password for user admin-larkinc: **********

    Name                           Value
    ----                           -----
    ComputerName                   PC0V3ZS4
    EncryptedDate                  6/05/2021 1:51:22 PM
    BitLockerKey                   114422-######-######-######-######-######-######-######

    VERBOSE: Active Directory queries complete

    This command gets the BitLocker key for the device that you specify from Active Directory as Verbose is specified additional information is written to the console
    #>

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $True)]
        [string] $computerName
    )
    
    begin {
        if (!(Get-Module ActiveDirectory)) {
            Import-Module ActiveDirectory
        }
    }
    
    process {
        Write-Verbose "Checking AD for $computerName's Distinguished Name"
        try {
            $everything_ok = $True
            $computer = (Get-ADComputer $computerName -ErrorAction Stop).DistinguishedName 
        }
        catch {
            $everything_ok = $false
            Write-Warning "No computer object found in DPE AD for $computerName check the spelling and try again`n"
        }
        
        if ($everything_ok) {
            Write-Verbose "Retrieving BitLocker unlock key from Active Directory"
            $obj = Get-ADObject -Filter 'objectClass -eq "msFVE-RecoveryInformation"' -SearchBase $computer -Properties whenCreated, msFVE-RecoveryPassword -Credential (Get-Credential) | Sort-Object whenCreated -Descending | Select-Object -First 1 #whenCreated, msFVE-RecoveryPassword

            [Ordered]@{
                ComputerName  = $computerName
                EncryptedDate = $obj.whenCreated
                BitLockerKey  = $obj.'msFVE-RecoveryPassword'
            }
            "`n"
            Write-Verbose "Active Directory queries complete"
        }
        
    }
    
    end {
        
    }
}