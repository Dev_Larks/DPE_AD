function Compare-DPE_DeviceGroups {

    <#
.SYNOPSIS
Compares the AD group membership of two computers and provides options to add selected AD groups from source computer to the new computer.

.DESCRIPTION
Compare-DPE_DeviceGroups uses Windows Active Directory (AD) to retrieve group information from two computers. Computers are specified by their Asset Name which is located on the desktop of each device. It then prints out information of the groups present on each device showing group memberships commonly shared, and uniquely present on each of the devices being compared to the console. 

== Symbolises AD groups present on both computers
=> Symbolises AD groups unique to the new computer
<= Symbolises AD groups unique to the old (source) computer

The option to add AD groups to the second device that is compared is provided via a graphical user interface if the addGroups parameter is specified.
AD Groups can only added to the computer specified by the newComputer parameter.

.PARAMETER sourceComputer
Computer name of the device the user was previously using.

.PARAMETER newComputer
Computer name of the new device the user is now using.

.PARAMETER addGroups
Specify this switch to present a graphical user interface that allows one or more groups to be selected to add to the users new computer.

.EXAMPLE
Compare-DPE_DeviceGroups -sourceComputer PC0V3MBM -newComputer PC0SFLH2

Name                                              SideIndicator
----                                              -------------
Domain Computers                                  ==
G-SE-Aruba-Computer                               ==
G-SE-OEH-DA-Windows8-DA-Clients                   ==
G-SE-CORP-INTUNE-COMANAGED-DEVICES                ==
G-SE-CORP-INTUNE-AOVPN-DEVICE-TUNNEL              ==
G-SE-CORP-INTUNE-AOVPN-USER-TUNNEL-v2.3           ==
G-SE-CORP-INTUNE-AOVPN-USER-TUNNEL-SSTP           =>
G-SE-App-SalesforceForOutlook_3.0.02.56_Uninstall <=
G-SE-App-GoogleEarthPro_7.3.1                     <=
G-SE-App-Touchpoint                               <=
G-SE-App-SCCM_2012R2_Console                      <=

#>

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $True, 
            HelpMessage = "Asset Name of the old device")]
        [Alias('oldLaptop')]
        [string] $sourceComputer,

        [Parameter(Mandatory = $True, 
            HelpMessage = "Asset Name of the new device")]
        [Alias('newLaptop')]
        [string] $newComputer,

        [switch] $addGroups
    )
    
    begin {
        if (!(Get-Module ActiveDirectory)) {
            Import-Module ActiveDirectory
        }

        $domain = ((Get-ADDomain).Forest)
    }
    
    process {
        Write-Verbose "Getting AD group membership of $sourceComputer"
        try {
            $everything_ok = $True
            $sourceDeviceMembership = Get-ADPrincipalGroupMembership $sourceComputer$ -ErrorAction Stop | Select-Object name
        }
        catch {
            $everything_ok = $False
            Write-Warning "$sourceComputer not found in $domain check spelling and try again"
        }
        
        if ($everything_ok) {
            Write-Verbose "Getting AD group membership of $newComputer"
            try {
                $newDeviceMembership = Get-ADPrincipalGroupMembership $newComputer$ -ErrorAction Stop | Select-Object name
            }
            catch {
                $everything_ok = $False
                Write-Warning "$newComputer not found in $domain check spelling and try again"

                Break
            }
            
            # Compare device groups and write to console
            Write-Verbose "Comparing $sourceComputer and $newComputer AD group memberships"
            $groups = Compare-Object ($sourceDeviceMembership) ($newDeviceMembership) -Property Name -IncludeEqual

            $groups | Out-String

            if ($addGroups) {
                
                $groupsToAdd = ($groupsToAdd = $groups | Out-Gridview -Title "AD groups: == present on both, => unique to new computer, <= unique to old computer" -passthru).Name
                Write-Verbose "Adding selected AD groups to $newComputer"
                if ($groupsToAdd) {
                    
                    $credentials = Get-Credential

                    foreach ($group in $groupsToAdd) {
                        Add-ADGroupMember -Identity $group -Members $newComputer$ -Credential $credentials
                    }
                }
                Write-Verbose "Selected AD groups now added to new computer"
            }
        
        }
    }
    
    end {
        
    }
}