# DPE_AD PowerShell module Change log

Version 0.4.4 - Add support for Windows 22H2 to Get-DPE_W10Version function.

Version 0.4.3 - Add colour to information written to console when adding missing AD groups Get-DPE_VPNConfigurationState function.

Version 0.4.2 - Update Get-DPE_VPNConfigurationState function for v2.6".

Version 0.4.1 - Remove 'G-SE-CORP-User-Cert' user AD group from Get-DPE_VPNConfigurationState function.

Version 0.4.0 - Add new Compare-DPE_DeviceGroup function to query and compare device AD membership.

Version 0.3.2 - Add support for Windows 21H2 in Get-DPE_W10Version function.

Version 0.3.1 - Fix bug in Get-UserID private function when searching by surname, user SAMAccountName not returned.

Version 0.3.0 - Add Get-DPE_BitLockerKey function.

Version 0.2.0 - Initial release of DPE_AD module.

