# What is DPE_AD

## Description
Created to support commonly executed tasks querying Active Directory (AD) for both user, computer, and group information.

## Author
Authored by Dev_Larks

## Overview
This module consists of PowerShell functions for tasks that are commonly executed when working in the Remote Support team. A brief description of the individual functions are below:

* Compare-DPE_DeviceGroups - compares the AD groups of two devices, adds any selected groups.
* Get-DPE_AccountStatus - retrieves key user AD account info, password status, locked out etc.
* Get-DPE_AppInfo - queries AD groups that match provided value, outputs description & info fields.
* Get-DPE_BitLockerKey - queries AD and returns last recorded BitLocker unlock key for a device.
* Get-DPE_VPNConfigurationState - queries user and device 'DPE - Always On VPN' AD membership, can remediate missing groups
* Get-DPE_W10Version - queries AD and returns device Windows 10 build version information.

## Installation
### Manual
1. Create the below folder structure and place 'DPE_AD' folder in 'Modules' folder  
C:\Users\\*YOURUSERID*\Documents\WindowsPowerShell\Modules\DPE_AD

2. Download DPE_AD module .zip file from <a href="https://codeberg.org/Dev_Larks/craig.dev.DPE_AD/src/branch/production" target="_blank">here</a>  
Extract the files and place the contents of the dpe_ad\Output\0.4.2 folder into the DPE_AD folder created in step 1.  
Click Yes if prompted to overwrite any existing versions of the files.

3. Launch Powershell  
   Type in 'Get-Module -Name DPE_AD' and press Enter the following output should be returned.  

   | ModuleType |Version    |Name                                |ExportedCommands|
   | ---------- |-------    |----                                |----------------|
   | Script     |0.4.2      |DPE_AD                              |{Compare-DPE_DeviceGroups, Get-DPE_AccountStatus, Get-DPE_AppInfo, Get-...|  

   </br>
   Type in 'Get-Command -Module DPE_AD' and press Enter the following output should be returned.

    |CommandType    |Name                                              |Version   |Source |
    |-----------    |----                                              |-------   |------ | 
    |Function       |Compare-DPE_DeviceGroups                          |0.4.2     |DPE_AD |
    |Function       |Get-DPE_AccountStatus                             |0.4.2     |DPE_AD |
    |Function       |Get-DPE_AppInfo                                   |0.4.2     |DPE_AD |
    |Function       |Get-DPE_BitLockerKey                              |0.4.2     |DPE_AD |
    |Function       |Get-DPE_VPNConfigurationState                     |0.4.2     |DPE_AD |
    |Function       |Get-DPE_W10Version                                |0.4.2     |DPE_AD |

    </br>
## Examples
Compare-DPE_DeviceGroups -sourceComputer PC0V3MBM -newComputer PC0SFLH2

    Name                                              SideIndicator
    ----                                              -------------
    Domain Computers                                  ==
    G-SE-Aruba-Computer                               ==
    G-SE-OEH-DA-Windows8-DA-Clients                   ==
    G-SE-CORP-INTUNE-COMANAGED-DEVICES                ==
    G-SE-CORP-INTUNE-AOVPN-DEVICE-TUNNEL              ==
    G-SE-CORP-INTUNE-AOVPN-USER-TUNNEL-v2.3           ==
    G-SE-CORP-INTUNE-AOVPN-USER-TUNNEL-SSTP           =>
    G-SE-App-SalesforceForOutlook_3.0.02.56_Uninstall <=
    G-SE-App-GoogleEarthPro_7.3.1                     <=
    G-SE-App-Touchpoint                               <=
    G-SE-App-SCCM_2012R2_Console                      <=

Get-DPE_AccountStatus -Usersname 'Craig Larkin'

    DisplayName           : Craig Larkin
    Username              : larkinc
    Department            : Enabling Technology
    Enabled               : True
    Locked Out            : False
    Password Expired:     : False
    Password Last Set:    : 22/04/2022 7:50:04 AM
    Password Expiry Date: : 21/07/2022 7:50:04 AM
    Bad Password Time:    : 1/01/1601 11:00:00 AM
    Bad Password Count:   : 0
    Last Logon:           : 1/01/1601 11:00:00 AM

Get-DPE_AppInfo

    Enter the group name or wildcard search string: Inreach

    GroupName        Description Info
    ---------        ----------- ----
    G-SE-App-inReach For CM9     Install on DPE staff computers only.

Get-DPE_BitLockerKey -computerName PC0V3ZS4 -Verbose

    Name                           Value
    ----                           -----
    ComputerName                   PC0V3ZS4
    EncryptedDate                  6/05/2021 1:51:22 PM
    BitLockerKey                   114422-######-######-######-######-######-######-######

Get-DPE_VPNConfigurationState -userName 'Craig Larkin' -computerName PC0V3MBM -noRemediate -Verbose

    VERBOSE: Querying Craig Larkin's VPN settings
    VERBOSE: Checking for user accounts that match Craig Larkin
    VERBOSE: Select the user name from the output window to continue

    VERBOSE: Querying AD for Craig Larkin's 'DPE - Always On VPN' configuration state
    G-SE-CORP-INTUNE-WIN10-Enrol-Users AD group present
    G-SE-CORP-VPN-Users AD group present

    VERBOSE: Querying AD for PC0NJ7ZA's 'DPE - Always On VPN' configuration state
    WARNING: G-SE-CORP-INTUNE-AOVPN-DEVICE-TUNNEL-Prod AD group missing
    WARNING: G-SE-CORP-INTUNE-AOVPN-USER-TUNNEL-Prod AD group missing
    G-SE-CORP-INTUNE-COMANAGED-DEVICES AD group present

    VERBOSE: Querying AD for PC0NJ7ZA's OU location
    PC0NJ7ZA is in the correct OU: 'Hybrid-Joined' for 'DPE - Always On VPN'

    
Get-DPE_W10Version -computerName PC0V3MBM

    Windows 10 21H2